import argparse
import asyncio
import datetime
from itertools import groupby
from operator import itemgetter
from urllib.parse import urlparse

from aiohttp import ClientSession


async def fetch(session, url):
    async with session.get(url) as response:
        return await response.text()


async def fetch_all(session, urls):
    start = datetime.datetime.now()
    results = await asyncio.gather(
        *[fetch(session, url) for url in urls],
        return_exceptions=True
    )

    total = len(results)
    results_and_netloc = zip(results, [urlparse(x).netloc for x in urls])
    errors = sorted([x for x in results_and_netloc if isinstance(x[0], Exception)], key=itemgetter(1))
    all_errors_by_netloc = {n: len(list(g)) for n, g in groupby(errors, key=itemgetter(1))}
    errors_by_netloc = {k: v for k, v in all_errors_by_netloc.items() if v > 0}

    print(f'Total: {total}, Errors: {len(errors)} {errors_by_netloc}')
    end = datetime.datetime.now()
    print(f'Took {end - start}')


async def main(args, loop):
    endpoints = [
        '/',
        '/users',
        '/users/admin',
        '/data/secrets',
        '/login',
        '/register',
        '/logout',
        '/passwords',
        '/admin',
        '/keys',
    ]
    ports = [
        5000,
        5100,
        5200,
    ]
    async with ClientSession(loop=loop) as session:
        urls = [
                   f'http://localhost:{port}{endpoint}'
                   for port in ports for endpoint in endpoints
               ] * args.scale
        while True:
            await fetch_all(session, urls)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate some flask load.')
    parser.add_argument('--scale', type=int, default=10, required=False, help='Load scale')
    args = parser.parse_args()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(args, loop))
