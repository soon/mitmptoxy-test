#Installation

1. `docker network create elk-services`
1.  `docker-compose up`
1.  (new console) `cd docker-elk` and then `docker-compose up`
1. Wait until kibana is available (a minute or so) and open http://localhost:5601
1. Enter elastic / changeme 
1. Open http://localhost:5000 and make sure it responds with `secret`
1. ^ the line above should also add an entry to logstash. This is required to create index pattern in kibana
1. http://0.0.0.0:5601/app/kibana#/management/kibana/index_pattern?_g=()
1. Build index with pattern like `logstash-2019.08*`. Make sure it matches at least one index
1. Open and inspect log data 

# Server installation
1. Make sure to block all unwanted ports
1. Use https://github.com/chaifeng/ufw-docker if you're using ufw
1. Use command `sudo ./ufw-docker allow mitmptoxytest_go-proxy_1 5000` where `5000` is the container port (not a mapped host port) to allow container `mitmptoxytest_go-proxy_1` to be exposed
1. Use command `sudo ./ufw-docker delete allow mitmptoxytest_go-proxy_1 5000` to remove rule

#Features:
1. Saves elastic data between restarts


# Customization

To reduce amount of fields change `Meta fields` in `/app/kibana#/management/kibana/settings/?_g=()`

To hide some fields in _source use "Index Patterns > Source filters" 

Index pattern must be unique, but you can easily add comments to them using "logstash,-headers" syntax. 
This line includes logstash index and excludes headers index and effectively the same as just "logstash", 
but allows you to mark this index pattern as an index without headers. 
Note that this only "marks" and does not actually filter index fields. To filter fields you still need to use 
"Index patterns > Source filters" 

Проблемы:

1. Нельзя чтобы кто-то видел наши логи
1. Нельзя чтобы они жрали много памяти / диска
1. Проблемы с индексом когда меняется формат данных
1. Как узнать что наш логгер падает?
1. Менять пароли


py-spy + mitmproxy

    $ echo 'manylinux1_compatible = True' > /usr/lib/python3.6/site-packages/_manylinux.py
    $ pip3 install py-spy
    $ ps
    $ py-spy --nonblocking --pid <HERE COMES PID> 