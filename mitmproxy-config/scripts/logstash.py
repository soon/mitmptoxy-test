import os
import queue
import threading

import requests
from mitmproxy import http
from mitmproxy.utils.human import format_address


send_queue = queue.Queue()


def send_data_to_logstash(data):
    try:
        requests.post("http://logstash:31311", json=data, headers={}, timeout=5)
    except Exception as e:
        if 'is not JSON serializable' in str(e):
            requests.post("http://logstash:31311", data=data, headers={}, timeout=5)
        else:
            raise


def send_thread_run(*args, **kwargs):
    while True:
        payload = send_queue.get()

        try:
            send_data_to_logstash(payload)
        except Exception as e:
            print("Error sending to logstash: ", e)
        send_queue.task_done()


def load(l):
    t = threading.Thread(target=send_thread_run, daemon=True)
    t.start()


def try_decode(value):
    try:
        return value.decode('utf-8')
    except:
        return value


def convert_headers(headers):
    return dict((try_decode(x), try_decode(y)) for x, y in headers.items())


def response(flow: http.HTTPFlow) -> None:
    try:
        payload = {
            'app': os.environ.get('APP_NAME'),
            'request_headers': convert_headers(flow.request.headers),
            'request_data': try_decode(flow.request.content),
            'timestamp': flow.request.timestamp_start,
            'response_data': try_decode(flow.response.content),
            'response_headers': convert_headers(flow.response.headers),
            'status_code': flow.response.status_code,
            'url': flow.request.pretty_url,
            'method': flow.request.method,
            'client_address': format_address(flow.client_conn.address),
        }
    except Exception as e:
        print("Error: ", e)
        return

    send_queue.put(payload)
